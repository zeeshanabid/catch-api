FROM golang:alpine

WORKDIR /go/src/catch
COPY . /go/src/catch
RUN apk update && \
    apk add glide && \
    apk add git && \
    glide install

RUN go build -o catch-api .
CMD ["./catch-api"]
