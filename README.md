# catch-api

A simple REST api to create catches.

## Requirements

- docker
- docker-compose
- curl or any web client


## Usage

Run api in docker container. `docker-compose up api`

Run tests in docker container. `docker-compose up test`

## Endpoints

### Create Catch
```
curl -XPOST \
-F 'username=bob' \
-F 'species=species' \
-F 'weight=1.12' \
-F 'length=1.12' \
-F 'latitude=59.1' \
-F 'longitude=54.1' \
-F 'picture=@/path/to/jpg' \
localhost:8080/catch
```

```
HTTP/1.1 200 OK
Content-Type: application/json; charset=utf-8
Date: Wed, 31 Jan 2018 18:40:21 GMT
Content-Length: 138

{"id":170,"username":"bob","species":"species","weight":1.12,"length":1.12,"latitude":59.1,"longitude":54.1,"created_at":"2018-01-31T18:40:21.839512Z"}
```



### List Catches

`curl localhost:8080/catch`

### Get Photo

`curl localhost:8080/catch/photo/:id`