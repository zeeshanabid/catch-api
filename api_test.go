package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"image"
	"image/jpeg"
	"io"
	"mime/multipart"
	"net/http"
	"net/http/httptest"
	"net/textproto"
	"os"
	"strconv"
	"strings"
	"testing"

	"github.com/go-zoo/bone"
)

func createController(t *testing.T) *CatchController {
	storage, err := NewCatchStorage()
	if err != nil {
		t.Fatal("Cannot create controller")
	}

	controller := &CatchController{Storage: storage}
	_, err = controller.Storage.DB.Exec("TRUNCATE catches")
	if err != nil {
		t.Errorf("Cannot truncate catches %s", err)
	}
	return controller
}

func escapeQuotes(s string) string {
	var quoteEscaper = strings.NewReplacer("\\", "\\\\", `"`, "\\\"")
	return quoteEscaper.Replace(s)
}

func readPicture() (img image.Image, err error) {
	file, err := os.Open("test_data/test_picture.jpg")
	if err != nil {
		return
	}

	img, err = jpeg.Decode(file)
	if err != nil {
		return
	}
	file.Close()

	return
}

func createMultipartForm(username, species string, weight float32) (b bytes.Buffer, ct string, err error) {
	w := multipart.NewWriter(&b)
	pictureName := "test_data/test_picture.jpg"
	fieldName := "picture"
	f, err := os.Open(pictureName)
	if err != nil {
		return
	}
	defer f.Close()

	h := make(textproto.MIMEHeader)
	h.Set("Content-Disposition",
		fmt.Sprintf(`form-data; name="%s"; filename="%s"`,
			escapeQuotes(fieldName), escapeQuotes(pictureName)))
	h.Set("Content-Type", "image/jpeg")

	fw, err := w.CreatePart(h)
	if err != nil {
		return
	}
	if _, err = io.Copy(fw, f); err != nil {
		return
	}

	err = w.WriteField("username", username)
	if err != nil {
		return
	}

	err = w.WriteField("species", species)
	if err != nil {
		return
	}

	err = w.WriteField("weight", strconv.FormatFloat(float64(weight), 'f', 3, 32))
	if err != nil {
		return
	}

	ct = w.FormDataContentType()
	w.Close()

	return
}

func TestCreateCatch(t *testing.T) {
	controller := createController(t)
	b, ct, err := createMultipartForm("bob", "new-species", 2.1)
	if err != nil {
		t.Errorf("Cannot create multipart form ({})", err)
	}
	req, err := http.NewRequest("POST", "/catch", &b)
	if err != nil {
		t.Errorf("Cannot create catch request")
	}
	req.Header.Set("Content-Type", ct)

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(controller.CreateCatch)
	handler.ServeHTTP(rr, req)

	if rr.Code != http.StatusOK {
		t.Errorf("Expected %d, got %d", http.StatusOK, rr.Code)
	}

	var catch Catch
	decoder := json.NewDecoder(rr.Body)
	decoder.Decode(&catch)

	if catch.Username != "bob" {
		t.Errorf("Expected username %s, got %s", "bob", catch.Username)
	}

	if catch.Species != "new-species" {
		t.Errorf("Expected species %s, got %s", "new-species", catch.Species)
	}
}

func TestCreateCatchWithoutUsername(t *testing.T) {
	controller := createController(t)
	b, ct, err := createMultipartForm("", "new-species", 2.1)
	if err != nil {
		t.Errorf("Cannot create multipart form ({})", err)
	}
	req, err := http.NewRequest("POST", "/catch", &b)
	if err != nil {
		t.Errorf("Cannot create catch request")
	}
	req.Header.Set("Content-Type", ct)

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(controller.CreateCatch)
	handler.ServeHTTP(rr, req)

	if rr.Code != http.StatusBadRequest {
		t.Errorf("Expected %d, got %d", http.StatusBadRequest, rr.Code)
	}
}

func TestListCatches(t *testing.T) {
	controller := createController(t)
	b, ct, err := createMultipartForm("bob", "species-1", 2.1)
	if err != nil {
		t.Errorf("Cannot create multipart form ({})", err)
	}
	req, err := http.NewRequest("POST", "/catch", &b)
	if err != nil {
		t.Errorf("Cannot create catch request")
	}
	req.Header.Set("Content-Type", ct)

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(controller.CreateCatch)
	handler.ServeHTTP(rr, req)

	b, ct, err = createMultipartForm("alice", "species-2", 4.1)
	if err != nil {
		t.Errorf("Cannot create multipart form ({})", err)
	}
	req, err = http.NewRequest("POST", "/catch", &b)
	if err != nil {
		t.Errorf("Cannot create catch request")
	}
	req.Header.Set("Content-Type", ct)

	rr = httptest.NewRecorder()
	handler = http.HandlerFunc(controller.CreateCatch)
	handler.ServeHTTP(rr, req)

	req, err = http.NewRequest("GET", "/catch", nil)
	rr = httptest.NewRecorder()
	handler = http.HandlerFunc(controller.ListCatches)
	handler.ServeHTTP(rr, req)

	if rr.Code != http.StatusOK {
		t.Errorf("Expected %d, got %d", http.StatusOK, rr.Code)
	}

	var catches []Catch
	decoder := json.NewDecoder(rr.Body)
	decoder.Decode(&catches)

	if len(catches) != 2 {
		t.Errorf("Expected %d, got %d", 2, len(catches))
	}

	if catches[0].Username != "alice" {
		t.Errorf("Expected %s, got %s", "alice", catches[0].Username)
	}

	if catches[1].Username != "bob" {
		t.Errorf("Expected %s, got %s", "bob", catches[1].Username)
	}
}

func TestResizePicture(t *testing.T) {
	controller := createController(t)
	b, ct, err := createMultipartForm("bob", "species-1", 2.1)
	if err != nil {
		t.Errorf("Cannot create multipart form ({})", err)
	}
	req, err := http.NewRequest("POST", "/catch", &b)
	if err != nil {
		t.Errorf("Cannot create catch request")
	}
	req.Header.Set("Content-Type", ct)

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(controller.CreateCatch)
	handler.ServeHTTP(rr, req)

	var catch Catch
	decoder := json.NewDecoder(rr.Body)
	decoder.Decode(&catch)

	mux := bone.New()
	mux.GetFunc("/catch/photo/:id", controller.GetPicture)
	server := httptest.NewServer(mux)
	defer server.Close()

	resp, err := http.Get(fmt.Sprintf("%s/catch/photo/%d", server.URL, catch.ID))

	if resp.StatusCode != http.StatusAccepted {
		t.Errorf("Expected %d, got %d", http.StatusAccepted, resp.StatusCode)
	}

	pic, err := readPicture()
	if err != nil {
		t.Errorf("Cannot read picture")
	}

	err = controller.resize(resizePicture{catch.ID, pic})
	if err != nil {
		t.Errorf("Cannot resize picture")
	}

	resp, err = http.Get(fmt.Sprintf("%s/catch/photo/%d", server.URL, catch.ID))

	if resp.StatusCode != http.StatusOK {
		t.Errorf("Expected %d, got %d", http.StatusOK, resp.StatusCode)
	}
}
