package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"image"
	"image/jpeg"
	"log"
	"net/http"
	"strconv"

	"github.com/go-zoo/bone"
	"github.com/nfnt/resize"
)

const (
	maxWidth  = 140
	maxHeight = 140
)

type CatchController struct {
	Storage    *CatchStorage
	resizeChan chan resizePicture
}

func (c CatchController) CreateCatch(w http.ResponseWriter, r *http.Request) {
	catch, img, err := parseCatch(r)
	if err != nil {
		writeResponse(w, http.StatusBadRequest, HttpError{"error", err.Error()})
		return
	}

	if len(catch.Username) == 0 {
		writeResponse(w, http.StatusBadRequest, HttpError{"error", "username cannot be empty"})
		return
	}

	err = c.Storage.CreateCatch(catch)
	if err != nil {
		writeResponse(w, http.StatusInternalServerError, HttpError{"error", "catch cannot be stored in database"})
		return
	}

	go func() {
		c.resizeChan <- resizePicture{id: catch.ID, picture: img}
	}()

	writeResponse(w, http.StatusOK, catch)
}

func (c CatchController) ListCatches(w http.ResponseWriter, r *http.Request) {
	catches, err := c.Storage.ListCatches()
	if err != nil {
		writeResponse(w, http.StatusInternalServerError, HttpError{"error", err.Error()})
		return
	}
	writeResponse(w, http.StatusOK, catches)
}

func (c CatchController) GetPicture(w http.ResponseWriter, r *http.Request) {
	idStr := bone.GetValue(r, "id")
	id, err := strconv.ParseInt(idStr, 10, 64)
	if err != nil {
		writeResponse(w, http.StatusInternalServerError, HttpError{"error", err.Error()})
		return
	}

	thumbnail, err := c.Storage.GetPictire(id)
	if err != nil {
		writeResponse(w, http.StatusInternalServerError, HttpError{"error", err.Error()})
		return
	}
	if thumbnail == nil {
		writeResponse(w, http.StatusAccepted, HttpError{"status", "thumbnail not ready yet"})
		return
	}

	w.Header().Set("Content-Type", "image/jpeg")
	w.Header().Set("Content-Length", strconv.Itoa(len(thumbnail)))
	if _, err := w.Write(thumbnail); err != nil {
		log.Println("Unable to write image.")
	}
}

func (c CatchController) resizeRoutine() {
	log.Println("Initializing resize routine...")
	for resizePicture := range c.resizeChan {
		log.Printf("Creating thumbnail for picture id %d", resizePicture.id)
		c.resize(resizePicture)
	}
}

func (c CatchController) resize(resizePicture resizePicture) error {
	b, err := createThumbnail(resizePicture.picture)
	if err != nil {
		log.Println("Cannot create thumbnail of the picture.")
		return err
	}

	err = c.Storage.StorePicture(resizePicture.id, b)
	if err != nil {
		log.Println("Cannot store picture to database.")
		return err
	}
	return nil
}

func writeResponse(w http.ResponseWriter, status int, body interface{}) {
	jsonBody, err := json.Marshal(body)
	if err != nil {
		log.Println("Cannot unmarshal body", err)
		status = http.StatusInternalServerError
	}
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(status)
	if body != nil {
		w.Write(jsonBody)
	}
}

func NewCatchController() (controller *CatchController, err error) {
	storage, err := NewCatchStorage()
	if err != nil {
		return
	}
	controller = &CatchController{
		Storage:    storage,
		resizeChan: make(chan resizePicture, 10),
	}
	go controller.resizeRoutine()
	return
}

func parseCatch(r *http.Request) (*Catch, image.Image, error) {
	username := r.FormValue("username")
	species := r.FormValue("species")
	weight, _ := strconv.ParseFloat(r.FormValue("weight"), 32)
	length, _ := strconv.ParseFloat(r.FormValue("length"), 32)
	latitude, _ := strconv.ParseFloat(r.FormValue("latitude"), 32)
	longitude, _ := strconv.ParseFloat(r.FormValue("longitude"), 32)

	picture, handle, err := r.FormFile("picture")
	if err != nil {
		return nil, nil, errors.New("picture must be set for the catch")
	}

	if handle.Header.Get("Content-Type") != "image/jpeg" {
		return nil, nil, errors.New("only jpeg file format is supported")
	}

	img, err := jpeg.Decode(picture)
	if err != nil {
		return nil, nil, errors.New("cannot decode jpeg image")
	}

	return &Catch{
		Username:  username,
		Species:   species,
		Weight:    float32(weight),
		Length:    float32(length),
		Latitude:  float32(latitude),
		Longitude: float32(longitude),
	}, img, nil
}

func createThumbnail(picture image.Image) (*bytes.Buffer, error) {
	thumbnail := resize.Thumbnail(maxWidth, maxHeight, picture, resize.Bicubic)
	b := new(bytes.Buffer)
	err := jpeg.Encode(b, thumbnail, nil)
	return b, err
}
