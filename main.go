package main

import (
	"log"
	"net/http"

	"github.com/go-zoo/bone"
)

func main() {
	mux := bone.New()

	controller, err := NewCatchController()
	if err != nil {
		panic(err)
	}

	mux.PostFunc("/catch", controller.CreateCatch)
	mux.GetFunc("/catch", controller.ListCatches)
	mux.GetFunc("/catch/photo/:id", controller.GetPicture)

	addr := ":8080"
	log.Println("Starting API on address", addr)
	err = http.ListenAndServe(addr, mux)
	if err != nil {
		panic(err)
	}
}
