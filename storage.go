package main

import (
	"log"
	"os"
	"time"

	"bytes"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

var schema = []string{
	`CREATE TABLE IF NOT EXISTS catches (
		id         SERIAL,
		username   TEXT,
		species    TEXT,
		weight     DOUBLE PRECISION,
		length     DOUBLE PRECISION,
		latitude   DOUBLE PRECISION,
		longitude  DOUBLE PRECISION,
		created_at TIMESTAMP DEFAULT now(),
		picture    BYTEA,

		CONSTRAINT pk_catches PRIMARY KEY(id)
	)`,
	`CREATE INDEX IF NOT EXISTS ix_catches_created_at
		ON catches(created_at)`,
}

type CatchStorage struct {
	DB *sqlx.DB
}

func NewCatchStorage() (*CatchStorage, error) {
	addr := os.Getenv("DB")
	quit := time.After(10 * time.Second)
	var err error
	var db *sqlx.DB
	for {
		select {
		case <-time.After(2 * time.Second):
			db, err = sqlx.Connect("postgres", addr)
			if err != nil {
				log.Println("Cannot connect to database. Retrying...", err)
				continue
			}
			catchStorage := &CatchStorage{
				DB: db,
			}
			err = catchStorage.initSchema()
			return catchStorage, err
		case <-quit:
			return nil, err
		}
	}
}

func (cs *CatchStorage) initSchema() error {
	for _, q := range schema {
		_, err := cs.DB.Exec(q)
		if err != nil {
			return err
		}
	}
	return nil
}

func (cs *CatchStorage) CreateCatch(catch *Catch) (err error) {
	q := `INSERT INTO catches (username, species, weight, length, latitude, longitude)
			VALUES (:username, :species, :weight, :length, :latitude, :longitude)
			RETURNING id, created_at`

	stmt, err := cs.DB.PrepareNamed(q)
	if err != nil {
		return
	}

	var c Catch
	err = stmt.Get(&c, catch)
	if err != nil {
		return
	}

	catch.ID = c.ID
	catch.CreatedAt = c.CreatedAt

	return
}

func (cs *CatchStorage) StorePicture(id int64, picture *bytes.Buffer) (err error) {
	q := `UPDATE catches SET picture = $1 WHERE id = $2`
	_, err = cs.DB.Exec(q, picture.Bytes(), id)
	return
}

func (cs *CatchStorage) GetPictire(id int64) (bytes []byte, err error) {
	q := `SELECT picture
			FROM catches
			WHERE id = $1`

	row := cs.DB.QueryRow(q, id)
	if err = row.Scan(&bytes); err != nil {
		return
	}

	return
}

func (cs *CatchStorage) ListCatches() (catches []Catch, err error) {
	q := `SELECT id, username, species, weight, length, latitude, longitude, created_at
			FROM catches
			ORDER BY created_at DESC`

	rows, err := cs.DB.Queryx(q)
	if err != nil {
		return
	}
	defer rows.Close()

	for rows.Next() {
		var catch Catch
		err = rows.StructScan(&catch)
		if err != nil {
			return
		}
		catches = append(catches, catch)
	}

	return
}
