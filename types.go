package main

import (
	"image"
	"time"
)

type Catch struct {
	ID        int64      `db:"id" json:"id"`
	Username  string     `db:"username" json:"username"`
	Species   string     `db:"species" json:"species"`
	Weight    float32    `db:"weight" json:"weight"`
	Length    float32    `db:"length" json:"length"`
	Latitude  float32    `db:"latitude" json:"latitude"`
	Longitude float32    `db:"longitude" json:"longitude"`
	CreatedAt *time.Time `db:"created_at" json:"created_at"`
}

type HttpError struct {
	Status string `json:"status"`
	Msg    string `json:"message"`
}

type resizePicture struct {
	id      int64
	picture image.Image
}
